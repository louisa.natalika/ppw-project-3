from django.test import TestCase, Client
from django.urls import resolve
from .views import about

# Create your tests here.

class UnitTestAbout(TestCase):
    def test_apakah_url_ada(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_menggunakan_fungsi_about(self):
        found = resolve('/')
        self.assertEqual(found.func, about)
    
    def test_apakah_fungsi_about_bekerja_saat_mendapat_request_GET(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('about.html')