$(document).ready(function(){
    $(".accordion-content").hide()
    $(".load").remove()
})

$(".slider").click(function(){
    if($(this).hasClass("light")){
        $(this).removeClass("light")
        $(this).addClass("dark")
        $(".polka").attr("src", "https://files.catbox.moe/he96cp.png")
        $(".style").attr("href","static/css/style2.css")
    } else{
        $(this).removeClass("dark")
        $(this).addClass("light")
        $(".polka").attr("src", "https://files.catbox.moe/uyaws1.png")
        $(".style").attr("href","static/css/style.css")
    }
})

$(".accordion-header").click(function(){
    if($(this).hasClass("active")){
        $(this).removeClass("active")
        $(this).next(".accordion-content").slideUp()
    } else{
        $(".accordion-box .accordion-content").slideUp()
        $(".accordion-box .accordion-header").removeClass("active")
        $(this).addClass("active")
        $(this).next(".accordion-content").slideDown()
    }
})